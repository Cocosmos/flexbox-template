# FlexBox Grid Template

## Exercice HTML-CSS sur les grid pour le 28.02.2021

### Contraintes : 

1. Utiliser Flexbox
2. Ajouter des classes d'option comme alignement verticaux et horizontaux.
3. Ajouter des classes permettant de changer l'orientation des blocks.
Bonus. Ajouter des classes pour gérer l'ordre.
